package com.davidkaluta.bricks;

import java.util.TimerTask;

/**
 * A simple timer
 * @author David Kaluta
 * @version 6.0
 * @since 6.0
 */
class TimeHelper extends TimerTask {

    /** 
     * seconds passed
     * @since 6.0
     */
    public static int seconds = 0;

    /**
     * minutes passed
     * @since 6.0
     */
    public static int minutes = 0;

    /**
     * hours passed
     * @since 6.0
     */
    public static int hours = 0;


    /**
     * Reset values on new TimeHelper
     */
    public TimeHelper() {
        TimeHelper.seconds = 0;
        TimeHelper.minutes = 0;
        TimeHelper.hours = 0;
    }
    /**
     * Run a timer
     * @since 6.0
     */
    public void run() {
        if (seconds < 59)
            seconds++;
        else {
            seconds = 0;
            if (minutes < 59)
                minutes++;
            else {
                minutes = 0;
                hours += 1;
            }
        }
    }
}