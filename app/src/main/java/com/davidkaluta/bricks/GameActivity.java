package com.davidkaluta.bricks;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

/**
 * The Activity the game runs in
 * @author David Kaluta
 * @version 8.1
 * @since 0.0
 */
public class GameActivity extends AppCompatActivity {

    /**
     * a GameView for the game
     * @since 0.0
     */
    private GameView gameView;

    /**
     * x-coordinate for touch location
     * @since 1.2
     */
    private float xDown;

    /**
     * y-coordinate for touch location
     * @since 1.2
     */
    private float yDown;

    /**
     * prepare for a launch
     * @param savedInstanceState required for OnCreate
     * @since 0.0
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gameView = new GameView(this);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getDecorView().setSystemUiVisibility
        (View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        setContentView(gameView);
    }

    /**
     * hide the top bar
     * @param hasFocus if the app is in focus
     * @since 1.2
     */
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    /**
     * check if the screen is touched
     * @param  event a MotionEvent with a touch action
     * @return       true if the screen was touched, false otherwise
     * @since 1.2
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        int action = event.getActionMasked();

        switch (action) {
            case (MotionEvent.ACTION_DOWN):
                xDown = event.getX();
                yDown = event.getY();
                return true;
            case (MotionEvent.ACTION_UP):
                float xUp = event.getX();
                float yUp = event.getY();
                gameView.getKiller().setDx((xUp - xDown) / 20);
                gameView.getKiller().setDy((yUp - yDown) / 20);
                return true;
            default:
                return super.onTouchEvent(event);
        }
    }
}
