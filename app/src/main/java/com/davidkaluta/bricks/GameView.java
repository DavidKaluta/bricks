package com.davidkaluta.bricks;

import android.content.Context;

import java.util.ArrayList;
import java.util.Random;

import android.content.res.Resources;
import android.graphics.*;
import android.media.MediaPlayer;
import android.view.View;

import java.util.Timer;
import java.util.TimerTask;

/**
 * The View the game runs in
 * @author David Kaluta
 * @version 7.0
 * @since 0.0
 */
public class GameView extends View {

    /**
     * a Killer
     * @since 0.2
     */
    private Killer killer;

    /**
     * every brick in an ArrayList
     * @since 3.3
     */
    private ArrayList<Brick> bricks;

    /**
     * every dead brick in an ArrayList
     * @since 7.0
     */
    private ArrayList<DeadBrick> dead;

    /**
     * background image
     * @since 4.0
     */
    private Bitmap bg;

    /**
     * a timer
     * @since 5.1
     */
    private Timer timer;

    private ArrayList<MediaPlayer> mps;

    /**
     * initialize a new GameView
     * @param  context required for initialization, meant to be an Activity
     * @since 0.0
     */
    public GameView(Context context) {
        super(context);
        int deviceWidth = Resources.getSystem().getDisplayMetrics().widthPixels;
        int deviceHeight = Resources.getSystem().getDisplayMetrics().heightPixels;
        bg = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(),
                R.drawable.background),
                deviceWidth, deviceHeight, true);
        spawnKiller();
        bricks = new ArrayList<>();
        dead = new ArrayList<>();
        TimerTask task = new TimeHelper();
        timer = new Timer();
        timer.schedule(task, 1000, 1000);
    }

    /**
     * create 6 bricks and put them in an ArrayList
     * @param bricks empty ArrayList to put the Bricks in
     * @since 4.1
     */
    public void spawnBricks(ArrayList<Brick> bricks) {
        if (bricks.isEmpty()) {
            Random random = new Random();
            Bitmap bmp = BitmapFactory.decodeResource(getResources(),
                    R.drawable.brick);
            float deviceWidth = Resources.getSystem().getDisplayMetrics()
                    .widthPixels;
            float deviceHeight = Resources.getSystem().getDisplayMetrics().
                    heightPixels;
            for (int i = 0; i < 6; i++) {
                bricks.add(new Brick(random.nextInt((int)
                        (deviceWidth - Brick.getWidth() - 10)) + 5,
                        random.nextInt((int)
                                (deviceHeight - Brick.getHeight() - 10)) + 5, this));
            }
        }
    }

    /**
     * Get the current bricks ArrayList
     * @return Bricks ArrayList
     * @since 3.3
     */
    public ArrayList<Brick> getBricks() {
        return bricks;
    }

    /**
     * replace the bricks ArrayList
     * @param bricks a new ArrayList
     * @since 3.3
     */
    public void setBricks(ArrayList<Brick> bricks) {
        this.bricks = bricks;
    }

    /**
     * Remove a dead brick from the relevant ArrayList
     * @param deadBrick A dead brick you want to remove
     * @since 7.0
     */
    public void removeDead(DeadBrick deadBrick) {
        dead.remove(deadBrick);
    }

    /**
     * Get the DeadBrick ArrayList
     * @return DeadBrick ArrayList
     * @since 7.0
     */
    public ArrayList<DeadBrick> getDead() {
        return dead;
    }

    /**
     * Modify the DeadBrick ArrayList
     * @param dead ArrayList
     * @since 7.0
     */
    public void setDead(ArrayList<DeadBrick> dead) {
        this.dead = dead;
    }

    /**
     * Spawn a new killer
     * @since 3.5
     */
    public void spawnKiller() {
        int deviceWidth = Resources.getSystem().getDisplayMetrics().widthPixels;
        int deviceHeight = Resources.getSystem().getDisplayMetrics().heightPixels;
        killer = null;
        killer = new Killer((float) deviceWidth / 2,
                (float) (deviceHeight) / 4 * 3, 0, 0, this);
    }

    /**
     * Get the killer
     * @return killer
     * @since v1.2
     */
    public Killer getKiller() {
        return killer;
    }

    /**
     * Draw everything
     * @param canvas a Canvas to draw on
     * @since 0.0
     */
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawBitmap(bg, 0, 0, null);
        for (Brick brick : bricks) {
            if (brick != null)
                brick.draw(canvas);
        }
        for (DeadBrick deadBrick: dead) {
            if(deadBrick != null)
                deadBrick.draw(canvas);
        }
        if (killer != null)
            killer.draw(canvas);
        Paint paint = new Paint();
        paint.setTextSize(50);
        paint.setColor(Color.WHITE);
        canvas.drawText(getContext().getString(R.string.score_textview,
                killer.getScore()), 50, 100, paint);
        canvas.drawText(getContext().getString(R.string.high_score_textview,
                Saver.getHighScore(getContext())),
                50, 150, paint);
        canvas.drawText(getContext().getString(R.string.timer_text,
                TimeHelper.hours, TimeHelper.minutes, TimeHelper.seconds),
                50, 200, paint);
        invalidate();
    }
}
