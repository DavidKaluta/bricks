package com.davidkaluta.bricks;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * A class for saving
 * @author David Kaluta
 * @version 5.0
 * @since 5.0
 */
public class Saver {

    /**
     * get the current high score
     * @param  context a Context, preferably an Activity to look at
     * @return         the current high score
     * @since 5.0
     */
    public static int getHighScore(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
        context.getString(R.string.preference_file_key),
            Context.MODE_PRIVATE);
        int defaultValue = -1;
        int highScore = sharedPref.getInt(context.getString(
        R.string.saved_high_score_key),
         defaultValue);
        return highScore;
    }

    /**
     * Set a new high score
     * @param context   a Context, preferably an Activity to look at
     * @param highScore the new High Score
     * @since 5.0
     */
    public static void setHighScore(Context context, int highScore) {
        SharedPreferences sharedPref = context.getSharedPreferences(
        context.getString(R.string.preference_file_key),
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(context.getString(R.string.saved_high_score_key), highScore);
        editor.commit();
    }
}
