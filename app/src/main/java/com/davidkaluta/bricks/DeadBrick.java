package com.davidkaluta.bricks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

/**
 * What bricks become after death
 * @author David Kaluta
 * @version 8.2
 * @since 7.0
 */
public class DeadBrick extends Brick implements Runnable {

    /**
     * dead brick's bitmap
     * @since 7.0
     */
    private Bitmap bmp;

    /**
     * Thread for the animations
     * @since 7.0
     */
    private Thread thread;

    /** 
     * dead brick's width
     * @since 7.0
     */
    private int w;

    /**
     * dead brick's height
     * @since v7
     */
    private int h;

    /**
     * GameView to run functions
     * @since v7
     */
    private GameView gv;

    /**
     * create a new DeadBrick from a Brick
     * @param  brick the brick you want to clone
     * @param  gv    a GameView for functions
     */
    public DeadBrick(Brick brick, GameView gv) {
        super(brick.x, brick.y, gv);
        this.gv = gv;
        w = (int) width;
        h = (int) height;
        bmp = BitmapFactory.decodeResource(gv.getResources(), R.drawable.brick);
        thread = new Thread(this, "deadThread");
        thread.start();
    }


    /**
     * Make the dead brick shrink
     * @since 7.0
     */
    @Override
    public void run() {
        while(w != 1) {
            bmp = Bitmap.createScaledBitmap(bmp, --w, --h, true);
            x+=0.5;
            y+=0.5;
            try {
                Thread.sleep(8);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        gv.removeDead(this);
    }

    /**
     * Draw a dead brick
     * @param canvas required
     */
    @Override
    public void draw(Canvas canvas) {
        canvas.drawBitmap(bmp, x, y, null);
    }
}
