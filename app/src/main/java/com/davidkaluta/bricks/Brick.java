package com.davidkaluta.bricks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

/**
 * The main target, and father to all entities
 * @author David Kaluta
 * @version 7.0
 * @since 0.0
 */
public class Brick {

    /**
     * the brick's bitmap
     * @since 0.0
     */
    private Bitmap bmp;

    /**
     * brick's x-coordinate
     * @since 0.5
     */
    protected float x;

    /**
     * brick's y-coordinate
     * @since 0.5
     */
    protected float y;

    /**
     * brick's width
     * @since 4.1
     */
    protected static float width;

    /**
     * brick's height
     * @since 4.1
     */
    protected static float height;
    
    /**
     * create a new brick
     * @param  x        x-coordinate for brick
     * @param  y        y-coordinate for brick
     * @param  gameView GameView for functions
     * @since 0.0
     */
    public Brick(float x, float y, GameView gameView) {
        bmp = BitmapFactory.decodeResource(gameView.getResources(),
         R.drawable.brick);
        width = bmp.getWidth();
        height = bmp.getHeight();
        this.x = x;
        this.y = y;
    }

    /**
     * get the width
     * @return width of a Brick
     * @since 4.1
     */
    public static float getWidth() {
        return width;
    }

    /**
     * get the x-coordinate of a specific Brick
     * @return x-coordinate of a specific Brick
     * @since 1.4
     */
    public float getX() {
        return x;
    }

    /**
     * get the x-coordinate of a specific Brick
     * @return x-coordinate of a specific Brick
     * @since 1.4
     */
    public float getY() {
        return y;
    }

    /**
     * get the height
     * @return height of a Brick
     * @since 4.1
     */
    public static float getHeight() {
        return height;
    }

    /**
     * draw the brick
     * @param canvas a canvas to draw the brick on
     * @since 0.0
     */
    public void draw(Canvas canvas) {
        canvas.drawBitmap(bmp, x, y, null);
    }
}
