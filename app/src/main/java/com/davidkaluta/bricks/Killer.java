package com.davidkaluta.bricks;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.media.MediaPlayer;
import android.provider.MediaStore;

import java.util.ArrayList;

/**
 * Your playable character
 * @author David Kaluta
 * @version 8.3
 * @since 0.2
 */
public class Killer extends Brick implements Runnable {

    /**
     * The killer's bitmap
     * @since 0.2
     */
    private Bitmap bmp;

    /**
     * The thread that runs the killer
     * @since 1.0
     */
    private Thread thread;

    /**
     * velocity, x-axis
     * @since 1.0
     */
    private float dx;

    /**
     * velocity, y-axis
     * @since 1.0
     */
    private float dy;

    /**
     * acceleration, x-axis
     * @since 4.3
     */
    private float ddx;

    /**
     * acceleration, y-axis
     * @since 4.3
     */
    private float ddy;

    /**
     * score
     * @since 5.0
     */
    private int score;

    /**
     * a GameView to run functions
     * @since 1.4
     */
    private GameView gv;

    /**
     * Create a new killer
     * @param  x        killer's x-location
     * @param  y        killer's y-location
     * @param  dx       killer's starting x-axis velocity
     * @param  dy       killer's starting y-axis velocity
     * @param  gameView a GameView for functions
     */
    public Killer(float x, float y, float dx, float dy, GameView gameView) {
        super(x, y, gameView);
        bmp = BitmapFactory.decodeResource(gameView.getResources(),
         R.drawable.ninjastar);
        gv = gameView;
        width = bmp.getWidth();
        height = bmp.getHeight();
        this.dx = dx;
        this.dy = dy;
        score = 0;
        thread = new Thread(this, "killerThread");
        thread.start();
    }

    /**
     * Set a new x-axis velocity
     * @param dx x-axis velocity
     * @since 1.2
     */
    public void setDx(float dx) {
        this.dx = dx;
    }

    /**
     * Set a new y-axis velocity
     * @param dy y-axis velocity
     * @since 1.2
     */
    public void setDy(float dy) {
        this.dy = dy;
    }

    public float getDdx() {
        return ddx;
    }

    public void setDdx(float ddx) {
        this.ddx = ddx;
    }

    public float getDdy() {
        return ddy;
    }

    public void setDdy(float ddy) {
        this.ddy = ddy;
    }

    /**
     * Get current score
     * @return score
     * @since 5.0
     */
    public int getScore() {
        return score;
    }

    /**
     * Draw a killer
     * @param canvas a Canvas to draw the killer on
     * @since 0.2
     */
    public void draw(Canvas canvas) {
        canvas.drawBitmap(bmp, x, y, null);
    }

    /**
     * Main game loop
     * @since 1.0
     */
    @Override
    public void run() {
        while (true) {
            int deviceWidth = Resources.getSystem().getDisplayMetrics()
            .widthPixels;
            int deviceHeight = Resources.getSystem().getDisplayMetrics()
            .heightPixels;
            if (x <= 0 || x >= deviceWidth - width)
                dx = -dx;
            if(x < 0)
                x = 0;
            if(x > deviceWidth - width)
                x = deviceWidth-width;
            if (y <= 0 || y >= deviceHeight - height)
                dy = -dy;
            ArrayList<Brick> bricks = gv.getBricks();
            ArrayList<DeadBrick> dead = gv.getDead();
            for (Brick brick: bricks) {
                if(brick != null) {
                    if (x + width >brick.x
                            && x < brick.x + Brick.width
                            && y + height > brick.y
                            && y < brick.y + Brick.height) {
                        dead.add(new DeadBrick(brick, gv));
                        bricks.remove(brick);
                        score++;
                        MediaPlayer mPlayer = MediaPlayer.create(gv.getContext(), R.raw.deathsound);
                        if(mPlayer != null) {
                            mPlayer.setLooping(false);
                        }
                        mPlayer.seekTo(0);
                        mPlayer.start();
                        if(Saver.getHighScore(gv.getContext()) < score)
                            Saver.setHighScore(gv.getContext(), score);
                        break;
                    }
                }
            }
            ddy = Math.abs(dy*(float) (0.003));
            ddx = Math.abs(dx*(float) (0.003));
            if(Math.sqrt(Math.pow(ddy,2) + Math.pow(ddx, 2)) <= 0.00001) {
                ddy = 0;
                ddx = 0;
                dy = 0;
                dx = 0;
            }
            if(dx > 0)
                dx -= ddx;
            else if(dx < 0)
                dx += ddx;
            if(dy > 0)
                dy -= ddy;
            else if(dy < 0)
                dy += ddy;
            x += dx;
            y += dy;
            if(bricks.isEmpty())
                gv.spawnBricks(bricks);
            try {
                Thread.sleep(16);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
