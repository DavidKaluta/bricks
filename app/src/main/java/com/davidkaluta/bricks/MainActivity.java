package com.davidkaluta.bricks;

import android.content.Intent;
import android.view.View;
import android.view.Window;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Intent msi = new Intent(this, MusicService.class);
        startService(msi);
    }

    @Override
    protected void onDestroy() {
        final Intent msi = new Intent(this, MusicService.class);
        stopService(msi);
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        final Intent msi = new Intent(this, MusicService.class);
        stopService(msi);
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        final Intent msi = new Intent(this, MusicService.class);
        startService(msi);
    }

    public void goToGame(View view) {
        Intent intent = new Intent(this, GameActivity.class);
        startActivity(intent);
    }

    public void goToCredits(View view) {
        Intent intent = new Intent(this, CreditsActivity.class);
        startActivity(intent);
    }

    public void goToTilt(View view) {
        Intent intent = new Intent(this, TiltActivity.class);
        startActivity(intent);
    }

}
